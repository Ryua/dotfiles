-- plugins
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

if not vim.uv.fs_stat(lazypath) then
  print('Installing lazy.nvim....')
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
  print('Done.')
end

vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
  {'ibhagwan/fzf-lua'},
  {'chriskempson/base16-vim'},
  {'neovim/nvim-lspconfig'},
  {'williamboman/mason.nvim', build = ':MasonUpdate'},
  {'williamboman/mason-lspconfig.nvim'},
  {'hrsh7th/nvim-cmp'},
  {'hrsh7th/cmp-nvim-lsp'},
  {'hrsh7th/cmp-buffer'},
  {'hrsh7th/cmp-path'},
  {'hrsh7th/cmp-cmdline'},
  {'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'},
  {'L3MON4D3/LuaSnip'},
  {'saadparwaiz1/cmp_luasnip'},
  {'rafamadriz/friendly-snippets'},
  {'tpope/vim-fugitive'},
  {'lewis6991/gitsigns.nvim'},
})

-- general settings
vim.opt.signcolumn = 'yes'
vim.opt.compatible = false
vim.cmd('filetype plugin indent on')
vim.cmd('syntax on')
vim.opt.backspace = {'indent', 'eol', 'start'}
vim.opt.mouse = 'a'
-- tab
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.autoindent = true
vim.opt.smartindent = true
-- search
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
-- no temp files
vim.opt.undofile = false
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.shortmess:append('I')
-- visual
vim.opt.background = 'dark'
vim.cmd('colorscheme base16-default-dark')
vim.opt.wrap = false
vim.opt.number = true
vim.opt.scrolloff = 10
vim.opt.laststatus = 2
vim.opt.statusline = '%{FugitiveStatusline()} %<%{expand("%:p:h")} %= %m%t %l:%c %{&fileencoding?&fileencoding:&encoding}'

-- keymaps
vim.g.mapleader = " "
vim.keymap.set('n', '<C-s>', ':w<CR>', {silent = true})
vim.keymap.set('n', '<C-ç>', ':vsplit<CR>', {silent = true})
vim.keymap.set('n', '<C-n>', ':enew<CR>', {silent = true})
vim.keymap.set('n', '<C-x>', '<C-w>x', {silent = true})
vim.keymap.set('n', '<leader><Left>', '<C-w>h', {silent = true})
vim.keymap.set('n', '<leader><Right>', '<C-w>l', {silent = true})
vim.keymap.set('n', '<C-Up>', '<C-u>', {silent = true})
vim.keymap.set('n', '<C-Down>', '<C-d>', {silent = true})
vim.keymap.set('n', '<C-a>', 'I', {silent = true})
vim.keymap.set('n', '<C-e>', 'A', {silent = true})

-- git
require("gitsigns").setup()

-- fzf lua
vim.keymap.set('n', '<C-p>', ':FzfLua files<CR>', {silent = true})
vim.keymap.set('n', '<C-b>', ':FzfLua buffers<CR>', {silent = true})
vim.keymap.set('n', '<C-g>', ':FzfLua grep_project<CR>', {silent = true})

-- lsp
vim.api.nvim_create_autocmd('LspAttach', {
  callback = function(event)
    local opts = {buffer = event.buf}

    vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
    vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
    vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
    vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
    vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
    vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)
    vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
    vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
    vim.keymap.set({'n', 'x'}, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
    vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
  end,
})

local lspconfig_defaults = require('lspconfig').util.default_config
lspconfig_defaults.capabilities = vim.tbl_deep_extend(
  'force',
  lspconfig_defaults.capabilities,
  require('cmp_nvim_lsp').default_capabilities()
)

require('mason').setup({})
require('mason-lspconfig').setup({
  handlers = {
    function(server_name)
      require('lspconfig')[server_name].setup({})
    end,
  }
})

-- auto complete
local luasnip = require("luasnip")
local cmp = require('cmp')
require('luasnip.loaders.from_vscode').lazy_load()

cmp.setup({
  sources = {
    {name = 'nvim_lsp'},
    {name = 'luasnip', keyword_length = 2},
    {name = 'buffer' , keyword_length = 3},
  },
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  window = {
    completion = { winhighlight = "Normal:CmpNormal", },
    documentation = cmp.config.window.bordered({ winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,CursorLine:PmenuSel,Search:None", }),
  },
  mapping = cmp.mapping.preset.insert({
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
    ['<C-Space>'] = cmp.mapping.complete(),
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.locally_jumpable(1) then
        luasnip.jump(1)
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
  }),
})

cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  }),
  matching = { disallow_symbol_nonprefix_matching = false }
})

-- treesitter
require'nvim-treesitter.configs'.setup {
  ensure_installed = { "go", "python", "c", "lua", "bash" },
  sync_install = true,
  auto_install = false,
  indent = { enable = true },
  highlight = {
    enable = true,
    disable = { "vim", "markdown" },
    additional_vim_regex_highlighting = false,
  },
}

--smart close
local function smart_close()
  if vim.fn.winnr('$') == 1 then
    local buffers = vim.fn.filter(vim.fn.range(1, vim.fn.bufnr('$')), 'buflisted(v:val)')
    
    if #buffers <= 1 then
      vim.cmd('quit')
    else
      vim.cmd('bdelete')
    end
  else
    vim.cmd('close')
  end
end

vim.keymap.set('n', '<C-q>', smart_close, { noremap = true, silent = true })
