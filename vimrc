call plug#begin('~/.vim/plugged')
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } 
  Plug 'junegunn/fzf.vim'
  Plug 'chriskempson/base16-vim'
call plug#end()

" general
set nocompatible
filetype plugin indent on
syntax on 
set backspace=indent,eol,start
set ttymouse=sgr
set mouse=a
" tab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
" search
set ignorecase
set incsearch
set nohlsearch
" no temp files
set noundofile
set noswapfile
set nobackup
set shortmess+=I
" visual
set background=dark
colorscheme base16-default-dark
set nowrap
set number
set laststatus=2
set statusline+=%F
set scrolloff=10
" fzf
nnoremap <C-p> :Files<cr>
nnoremap <C-b> :Buffers<cr>
nnoremap <C-g> :Rg<cr>
let g:fzf_layout = {'down':'40%'}
" remaps
let mapleader=""
nnoremap <C-a> I
nnoremap <C-e> A
nnoremap <C-s> :w<cr>
nnoremap <C-v> :vsplit<cr>
nnoremap <C-n> :enew<cr>
nnoremap <C-x> <C-w>x 
nnoremap <leader><Left> <C-w>h
nnoremap <leader><Right> <C-w>l
nnoremap <C-Up> <C-u>
nnoremap <C-Down> <C-d>
" smart close 
function! SmartClose()
  if winnr('$') == 1
    if len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) <= 1
      quit
    else
      bdelete
    endif
  else
    close
  endif
endfunction

nnoremap <C-q> :call SmartClose()<CR>
